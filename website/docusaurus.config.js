// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'SiLA 2 Manager',
  tagline: 'A software platform for laboratory automation.',
  url: 'https://pages.gitlab.io',
  baseUrl: '/sila2_manager/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'biovt', // Usually your GitHub org/user name.
  projectName: 'sila2_manager', // Usually your repo name.

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          editUrl: 'https://gitlab.com/biovt/sila2_manager',
        },
        // blog: {
        //   showReadingTime: true,
        //   // Please change this to your repo.
        //   editUrl: 'https://gitlab.com/biovt/sila2_manager',
        // },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      navbar: {
        // title: 'Sila',
        logo: {
          alt: 'My Site Logo',
          src: 'img/sila-python-logo.png',
        },
        items: [
          {
            type: 'doc',
            docId: 'intro',
            position: 'left',
            label: 'Docs',
          },
          {
            type: 'doc',
            docId: 'Tutorial-and-Examples/WIP',
            position: 'left',
            label: 'Examples',
          },
          {
            type: 'doc',
            docId: 'API/api',
            position: 'left',
            label: 'API',
          },
          {
            href: 'https://gitlab.com/biovt/sila2lib_implementations',
            position: 'left',
            label: 'Device drivers',
          },
          // {to: '/blog', label: 'Blog', position: 'left'},
          {
            href: 'https://gitlab.com/biovt/sila2_manager',
            label: 'GitLab',
            position: 'right',
          },
        ],
      },
      footer: {
        style: 'dark',
        links: [
          {
            title: 'Docs',
            items: [
              {
                label: 'Tutorial',
                to: '/docs/intro',
              },
              {
                label: 'Old sila2-manager documentation',
                to: 'https://sila2-manager.readthedocs.io/en/latest/',
              },
            ],
          },
          {
            title: 'Community',
            items: [
              {
                label: 'Our SiLA2 device drivers',
                href: 'https://biovt.gitlab.io/sila2lib_implementations/',
              },
              {
                label: 'Sila consortium',
                href: 'https://sila-standard.com/about-us/',
              },
              {
                label: 'All Sila repositories',
                href: 'https://gitlab.com/SiLA2',
              },
            ],
          },
          {
            title: 'More',
            items: [
              // {
              //   label: 'Blog',
              //   to: '/blog',
              // },
              {
                label: 'GitLab',
                href: 'https://gitlab.com/biovt/sila2_manager',
              },
              {
                label: 'PyPi',
                href: 'https://pypi.org/project/sila2lib/',
              },
            ],
          },
        ],
        copyright: `Copyright © ${new Date().getFullYear()} Lukas Bromig, Institute of Biochemical Engineering, Technische Universität München.`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),
};

module.exports = config;
