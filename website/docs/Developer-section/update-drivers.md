---
sidebar_position: 5
---

# Update drivers
The idea of the generated folder is that there is no device specific files. Beause this folder is re-generated always and changes are discarded there. Therefore, always store all your configurations into the `feature_implementation` folder. The implementation can be regenereated safely. It is suggested to make backups of these files. Manually merge the backed up files (`server.py` and `__main__.py`) with the new generated.

:::info
Make sure to backup the `server.py` and `__main__.py` files since they will be re-generated and overwritten. Though, you made quite some changes to them before. Make sure not to loose them.
:::
## Changed .xml
Manually merge the backed up files (`server.py` and `__main__.py`) with the new generated.
blablabla, test newly implemented functions...write pytests for them...

## changed sila_python implementation
Manually merge the backed up files (`server.py` and `__main__.py`) with the new generated.
check functionality! run all pytests....