---
sidebar_position: 2
---

# Installation

The repository is setup in a way that it can be installed conveniently as python package.
In case of any failures make sure to read the troubleshooting section. There are two possible ways for an installation:

## Gitlab

Navigate into a directory where you would like to build the sila2lib-implementations. Clone the repository (or desired branches) and change into it. Install the package management tool poetry and configure it in a way to create the virtual environment in your project directory. This might be crutial to circumvent some import errors. With `poetry shell` you can activate the virtual environment where poetry installed your software in. Use `deactivate` to exit the activated venv again.  
Alternatively, pip or different methods listed [here](https://packaging.python.org/en/latest/tutorials/installing-packages/) can be used for installation. A `setup.py` and `requirements.txt` file is provided for these cases. However, they are not recommended.

``` {.sourceCode .console}
git clone https://gitlab.com/biovt/sila2lib_implementations
cd sila2lib_implementations

sudo apt update && sudo apt upgrade
pip install poetry
poetry config virtualenvs.in-project true

poetry install
poetry shell 

# with pip
pip install --editable .[dev]  # [dev] installs the development packages, too
```

## Quickstart - first steps

To verify if the installation was successful, check the User section (insert link) to run some commands. This way you also get an understanding of users needs and the demands towards the software.


# Package management

The packagemanagement as well as all its features like building and publishing are supposed to be accomplished with [Poetry](https://python-poetry.org/). Alternatively, pip or different methods listed [here](https://packaging.python.org/en/latest/tutorials/installing-packages/) can be used for installation. A `setup.py` and `requirements.txt` file is provided for these cases. These dependencies also get updated via the CI pipeline while publishing a new version. However, we would like to stay with Poetry.

Mention the `Poetry run command` issue vs. `python -m command`
poetry install --no-dev


## Pyproject.toml - Package harbourage

Next to the listing of dependencies and development dependencies, the `.toml` file also harbours build information and settings for the delevopment tools. These settings are parsed by the hooks if you use them. Hooks can be managed with `pre-commit` e.g. [link the section here].


> check commands there for old dependencies needed by outdated software as it accounts for Rasberry Pi OS Buster e.g. [compabilities](https://www.piwheels.org/project/grpcio/)
downgraded versions for Raspberry pi OS 'Buster': If necessary un-/comment:  
grpcio-tools = "==1.42.0"  
grpcio = "==1.42.0"  
cryptography = "==2.3"


## Useful Poetry commands
```
poetry shell
poetry update
poetry lock
poetry install
poetry install --no-dev
poetry run
poetry env list
poetry env info
```
:::tip Export dependency
```
poetry export --without-hashes -f requirements.txt -o requirements.txt
poetry export --dev --without-hashes --format=requirements.txt -o requirements-dev.txt
```
:::
\\do there exist pyenv export, too?


## Tips on Poetry usage

* Poetry does install the current project in editable mode on poetry install (like pip install -e .)
* After deleting a dependency manually in the .toml, run the following command to remove all subdependencies in the venv. Doublecheck if you did not remove dependencies that are actually required (happend to me once):
```
poetry install --remove-untracked
```
* In case of errors there is always the possibility to `pip install packages` in the existing poetry venv.
