---
sidebar_position: 4
---

# Service Manager (WIP)

Laboratory automation without device integration is not the real deal. To fully automate your workflows and acquire your
data, you need full control of your lab devices. We use SiLA 2 as device interface standard to accomplish that. The 
Service Manager is our device layer that handles the device access.

🚧 This is work in progress! 🚧