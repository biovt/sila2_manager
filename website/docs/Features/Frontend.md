---
sidebar_position: 1
---

# Frontend (WIP)

The frontend is the graphical user interface (GUI) of the SiLA 2 Manager application. The SiLA 2 Manager is
a web application and the GUI is accessed through your web browser from anywhere and any device you want. In this
chapter we will tell you about the frontend, what it is made of, what it consists of and how to use it.

🚧 This is work in progress! 🚧