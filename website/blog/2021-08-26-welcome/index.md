---
slug: welcome
title: Welcome
authors: [lukas, felix]
tags: [hello, Sila]
---

Do we want to maintain a blog?

![Sila](./sila-python-logo.png)


**And if you don't want a blog**: just delete this directory, and use `blog: false` in your Docusaurus config.
