import React from 'react';
import clsx from 'clsx';
import styles from './HomepageFeatures.module.css';

const FeatureList = [
  {
    title: 'Discover, manage, and control!',
    Svg: require('../../static/img/control-tower.svg').default,
    description: (
      <>
        Device integration is ...
        What to put here? Silas 3 main advantages? Some figures, or nothing at all?
        Here it the path to my config: src/components/HomepageFeatures.js
        // New pictures could be browsed here: 'https://undraw.co/illustrations'
      </>
    ),
  },
  {
    title: 'Data is key.',
    Svg: require('../../static/img/data.svg').default,
    description: (
      <>
        Docusaurus lets you focus on your docs, and we&apos;ll do the chores. Go
        ahead and move your docs into the <code>docs</code> directory.
      </>
    ),
  },
  {
    title: 'Design and execute your workflows!',
    Svg: require('../../static/img/design.svg').default,
    description: (
      <>
        Extend or customize your website layout by reusing React. Docusaurus can
        be extended while reusing the same header and footer.
      </>
    ),
  },
  {
        title: 'LIMS or LaMaS?',
    Svg: require('../../static/img/llama.svg').default,
    description: (
      <>
        What do you need in your lab?
      </>
    ),
  },
  {
    title: 'Service architecture',
    Svg: require('../../static/img/architecture-plan.svg').default,
    description: (
      <>
        What is our architectural advantage?
      </>
    ),
  },
  {
    title: 'Open-Source',
    Svg: require('../../static/img/binary-code.svg').default,
    description: (
      <>
        Play nice. Why open-source is the future of lab automation.
      </>
    ),
  },
  {
    title: 'Usability is key',
    Svg: require('../../static/img/ux-design.svg').default,
    description: (
      <>
        IoT, web-apps and change management...
      </>
    ),
  },
];

function Feature({Svg, title, description}) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        <Svg className={styles.featureSvg} alt={title} />
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
