import { CustomerLogo } from "./types"

export const logos: CustomerLogo[] = [
  {
    src: "/img/pages/customers/logos/TUM_Logo_blau_rgb_s.svg",
    alt: "TUM Logo",
    width: 140,
    height: 56,
  },
  {
    src: "/img/pages/customers/logos/unternehmertum.svg",
    alt: "UnternehmerTUM Logo",
    width: 140,
    height: 56,
  },
  {
    src: "/img/pages/customers/logos/BMBF_Logo.svg",
    alt: "BMBF Logo",
    width: 180,
    height: 90,
  },

]
