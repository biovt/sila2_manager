const consts = require("./consts")

module.exports = {
  artifactHubUrl: "https://artifacthub.io/packages/helm/questdb/questdb",
  copyright: `Copyright © ${new Date().getFullYear()} SiLA 2 Manager`,
  demoUrl: `https://demo.${consts.domain}`,
  description:
    "The SiLA 2 Manager is an open-source software for lab automation.",
  dockerUrl: "https://hub.docker.com/r/questdb/questdb",
  domain: consts.domain,
  githubOrgUrl: consts.gitlabOrgUrl,
  githubUrl: `${consts.gitlabOrgUrl}/sila2_manager`,
  helmVersion: "0.11.0",
  linkedInUrl: "https://www.linkedin.com/company/questdb/",
  oneLiner: "The SiLA 2 Manager for lab automation",
  slackUrl: `https://slack.${consts.domain}`,
  stackoverflowUrl: "https://stackoverflow.com/questions/tagged/questdb",
  twitterUrl: "https://twitter.com/questdb",
  videosUrl: "https://www.youtube.com/channel/UChqKEmOyiD9c6QFx2mjKwiA",
}
