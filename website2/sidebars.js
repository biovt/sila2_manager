let guidelines

if (process.env.NODE_ENV === "development") {
  guidelines = {
    label: "Guidelines (DEV ONLY)",
    type: "category",
    items: [
      {
        type: "category",
        label: "Templates",
        items: [
          "__guidelines/template/guide",
          "__guidelines/template/function",
          "__guidelines/template/sql",
        ],
      },
      "__guidelines/naming-convention",
      "__guidelines/content-hierarchy",
      "__guidelines/lexicon",
      "__guidelines/markdown",
      "__guidelines/sql-code-blocks",
      "__guidelines/influences",
    ],
  }
}

module.exports = {
  docs: [
    {
      id: "introduction",
      type: "doc",
    },
    {
      label: "Get Started",
      type: "category",
      items: [
        "get-started/download",
        "get-started/deploy",
        "get-started/service-discovery",
        "get-started/first-workflow",
      ],
    },
    {
      label: "Develop",
      type: "category",
      items: [
        "develop/starting",
        "develop/databases",
        "develop/used-tech",
      ],
    },
    {
      label: "Deployment",
      type: "category",
      items: [
      ],
    },
    {
      label: "Reference",
      type: "category",
      items: [
        "reference/overview",
        "reference/api",
        {
          type: "category",
          label: "Services",
          items: [
            "reference/services/backend-gateway",
            "reference/services/frontend",
            "reference/services/service-manager",
            "reference/services/scheduler",
            "reference/services/workflow-designer-nr",
            "reference/services/workflow-designer-py",
            "reference/services/data",
          ],
        },
      ],
    },
    "faq"
  ].filter(Boolean),
}
