---
title: Introduction
description:
  Introduction to the SiLA 2 Manager and overview of its documentation.
---

The SiLA 2 Manager is an open-source service-based software for the automation and data 
management of R&D processes in laboratories. While it is built to work out-of-the box 
and enable lab automation in minutes, it features an open architecture and easy-to-customize
functionality in order to fit in every laboratory.

## Use Cases

The SiLA 2 Manager has its origins in the orchestration and automation of bioprocesses but 
is designed to handle any Life Science or related workflows. 

The software can be used to automate routine tasks like instrument calibrations or pipetting 
work as well as orchestrate larger, long-term workflows.

## Target Group

The two main user groups of the SiLA 2 Manager are scientists who want a tool to automate work and
ensure data acquisition and storage while still being easy to use. 

Additionally, the software gives automation experts a framework to develop specialized solutions
and increase standardization and reuse while lowering costs and the barrier for entry.

## Roadmap

## Support us


## Contents of this Documentation

### Get Started

### Develop

### Reference

### FAQ
