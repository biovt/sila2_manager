# Sila2 Manager
This is the repository for the documentation, roadmap and vision of the SiLA 2 Manager. All this is hosted on Gitlab 
Pages under: [biovt.gitlab.io/sila2lib_manager/](https://biovt.gitlab.io/sila2lib_manager/) 
🚧 This is work in progress! The gitlab-ci.yml for the page deployment has not been pushed yet ;) 🚧

## Getting started

This README aims at users editing the SiLA 2 Manager website. General knowledge of GIT is assumed. 

### Local development

```
git clone
cd website
npm install
npm start
```
